# News Parsing Service with RabbitMQ

A News parsing service from a news resource using Symfony.


## Installation
- Start the service

```bash
docker-compose up -d
```

- Populate news with a Symfony parsenews

```bash
 docker-compose exec web_service  php  /var/www/project/bin/console Parsenews
```

- Open on http://localhost:8000/
  - admin
    - user: admin@test.com  
      password: 12345678
  - non-admin
    - user: moderator@test.com  
      password: 12345678

## Built on

- Symfony 5.4
- Php 7.4
- Mysql
- Bootstrap 5.1
- Docker (docker-compose)
- RabbitMQ
